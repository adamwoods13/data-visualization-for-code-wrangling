String date = "04/07/2013";

int day = 86400 ;
int month = 2629743 ;
int year = 31556926 ;

for(int i=0; i<1;i++){
  String set[] = splitTokens(date,"/");
  int d = Integer.parseInt(set[0]);
  int m = Integer.parseInt(set[1]);
  int y = Integer.parseInt(set[2]);
  int epochDay = d*day;
  int epochMonth = m*month;
  int epochYear = (y-1970)*year;
  int epoch = epochYear + epochMonth + epochDay;
  println(epochDay, epochMonth, epochYear, epoch);
}