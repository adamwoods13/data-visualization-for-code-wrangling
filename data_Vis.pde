String[] set = {};
String[] data = {};
String[] artist = {};
float[] rank = {};
int[] ranky = {};
int[] rankm = {};
int[] rankw = {};
int[] score = {};
int[] date = {};
int[] epoch = {};
PFont mono;
int bw = 18;
boolean up1 = false;
boolean up2 = false;
boolean up3 = false;
int ty = 30; //initialises variables. Should have been done in OOP.

void setup() {
  size(1000, 800);
  smooth();
  frameRate(30);
  mono = createFont("Lato-Bold.ttf", 14);
  data = loadStrings("datafile.txt"); //initialises data
  for(int i=0; i< data.length;i++){
    set = splitTokens(data[i],"|");
    artist = append(artist, set[0]);
    score = append(score, i);
    rank = append(rank, int(set[1]));
    ranky = append(ranky, int(set[1]));
    rankm = append(rankm, int(set[2]));
    rankw = append(rankw, int(set[3]));
    date = append(date, int(map(epochConvert(set[4]),1477872000,1519673888, 90, 255))); //splits data from a text file and appends it to arrays
  }
}

void draw() {
    background(20,30,70);
    fill(255);
    textSize(20);
    textAlign(CENTER);
    text("Four Weeks",width/2,ty+70);
    text("Six Month",width/2,ty+40);
    text("One Year",width/2,ty);
    fill(20,30,70);
    stroke(20,30,70);
    rect(width/2-60, 40, 120, 70); //basic screen stuff
    translate(width/bw, height/2); //translates for data
    textAlign(LEFT); //resets alignment
    rend(); //calls rend function
    change(); //calls change function
}
void change(){ //is called in draw
  if(up1 == true){ //if statement to change time period of data on screen.
    updatey(); //calls update function
    if(ty < 30){ //if statements to change time text
      ty++;
    } else if(ty > 30){
      ty--;
    }
  }
  if(up2 == true){
    updatem();
    if(ty > -10){
      ty--;
    } else if( ty < -10){
      ty++;
    }
  }
  if(up3 == true){
    updatew();
    if(ty > -40){
      ty--;
    } else if(ty < -40){
      ty++;
    }
  }
}
void keyPressed() { //called everytime a button is pressed
  if (key == 'a' && up1 == false) { //if statement to trigger change in time period in void change.
    up1 = true;
    up2 = false;
    up3 = false;
  } else if(key == 's' && up2 == false){
    up1 = false;
    up2 = true;
    up3 = false;
  } else if (key == 'd' && up3 == false){
    up1 = false;
    up2 = false;
    up3 = true;
  }
}
void updatey(){ //called in void change
  for(int i=0; i< date.length; i++){ //loops through data
    if (rank[i] < ranky[i]){ //if statement to change height of screen data to match required data of time period chosen
      rank[i] += 0.5; 
    } else if(rank[i] > ranky[i]){
      rank[i] -= 0.5;
    }
  }
}
void updatem(){
  for(int i=0; i< date.length; i++){
    if (rank[i] < rankm[i]){
      rank[i] += 0.5;
    } else if(rank[i] > rankm[i]){
      rank[i] -= 0.5;
    }
  }
}
void updatew(){
  for(int i=0; i< date.length; i++){
    if (rank[i] < rankw[i]){
      rank[i] += 0.5;
    } else if(rank[i] > rankw[i]){
      rank[i] -= 0.5;
    }
  }
}
  
void rend(){ //called in void draw
  fill(255); //basic screen stuffs
  for (int i=0; i < data.length; i++){ //loops through data
    noStroke();
    strokeWeight(15);
    stroke(date[i],0,100); //sets colour that is influenced by the date of i
    float h = map(rank[i],0,100,12,100)*5; //sets height based on the rank of i
    line(i*bw, 0-h, i*bw, 0+h); //renders data line based on i and h and modified so it looks good on the screen
    rotate(-PI/2); //rotates everything by 90 degrees anticlockwise
    fill(20,30,70);
    textFont(mono);
    text(artist[i], 0-h, i*bw+5); //draws text based on artist data of i
    text(round(rank[i]), 0+h-bw, i*bw+5); //draws number based on rank of i
    rotate(PI/2); //undo rotate
  }
}

int epochConvert(String date){ //converts date from data file to epoch time.
  
  int ds = 86400, ms = 2629743, ys = 31556926; //the relative conversion rate of days to seconds Name format: ds == day seconds. ms == month seconds. ys == year seconds.
  
  String single[] = splitTokens(date,"/"); //splits data from the date array given based on where it finds "/" in the String. Should plit into 3 parts day, month and year.
  int epoch = (Integer.parseInt(single[0])-1)*ds + (Integer.parseInt(single[1])-1)*ms + (Integer.parseInt(single[2])-1970)*ys; // calculates epoch by converting the days months and years from 1/1/1970 to epoch time.
  
  return epoch;
}